# Microsoft Hololens 1 controlling ESPHome device
> As a project in the context of [UAS Technikum Wien](https://www.technikum-wien.at/) Master Software Engineering course "Mixed Reality" MSE-BB-3-WS2021-MR

This repo demonstrates the capability of [Microsoft Hololens 1](https://docs.microsoft.com/en-us/hololens/hololens1-hardware) interacting with [ESPHome](https://esphome.io/). 

As a target device for ESPHome a [NodeMCUv2](https://components101.com/sites/default/files/component_datasheet/ESP8266-NodeMCU-Datasheet.pdf) was chosen with a temperature and barometric pressure sensor [Bosch BMP085](https://components101.com/sites/default/files/component_datasheet/BMP085-datasheet.pdf) and a single [WS2812B](https://pdf1.alldatasheet.com/datasheet-pdf/view/1179113/WORLDSEMI/WS2812B.html) Neopixel LED with RGB capabilities. Construction and flashin of the NodeMCUv2 will be omitted in this project since a working ESPHome device is expected as prerequisite.

## Features
Upon start of the Hololens Application, the user must place one of two available Vuforia markers on a surface visible to the Hololens Camera (courtesy of https://shawnlehner.github.io/ARMaker/):
- [Marker 1](Marker/marker_1.jpg)
- [Marker 2](Marker/marker_2.jpg)

Depending on the marker (in this case Marker 1) a UI board will appear. It immediatley will allow to communicate with the ESPHome device. In this case a UI pane with three sliders and a button appears. 

![Marker Placement](Documentation/marker_slider.gif)

The button will toggle the Neopixel LED while the sliders will adjust the individual color channels of the LED. Commands from the Sliders are sent to the device upon stopping the slide event: when releasing the slider knob.

![Color changes](Documentation/slider_colorchanges.gif)

The second marker will spawn a UI panel that fetches sensor values from the ESPHome device. The BMP085 sensor provides i2c access to temperature and atmospheric pressure values. These values are polled from the device when pressing the "refresh" button on the UI pane.

![Temperature and Pressure Sensor panel](Documentation/temp_pressure_indicator.gif)

## Stack

### Hardware
- Hololens 1
- NodeMCUv2
- BMP085
- WS2812B NeoPixel

### Software
- Unity 2020.3.18f1
- Vuforia 10.2.5
- MRTK 2.7.0
- ESPHome 16.0