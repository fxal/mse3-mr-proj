﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Resources
{
    [Serializable]
    public class SensorValues
    {
        public string id;
        public string state;
        public string value;
    }
}
