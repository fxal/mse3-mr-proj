using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.UI;

public class RemoteControlEsp32 : MonoBehaviour
    
{
    [SerializeField]
    private string toggleUrl;

    [SerializeField]
    private string colorUrl;

    [SerializeField]
    private PinchSlider channelRed;

    [SerializeField]
    private PinchSlider channelGreen;

    [SerializeField]
    private PinchSlider channelBlue;

    [SerializeField]
    private Interactable toggleButton;

    private int channelRedValue = 0;

    private int channelBlueValue = 0;

    private int channelGreenValue = 0;

    void Start()
    {
        this.toggleButton.OnClick.AddListener(delegate() { ToggleEsp32Light(); });
        this.channelRed.OnInteractionEnded.AddListener(delegate { OnSliderValueChanged(channelRed); });
        this.channelGreen.OnInteractionEnded.AddListener(delegate { OnSliderValueChanged(channelGreen); });
        this.channelBlue.OnInteractionEnded.AddListener(delegate { OnSliderValueChanged(channelBlue); });
    }

    public void ToggleEsp32Light()
    {
        Debug.Log("Toggling ESP32 Light");
        WWWForm form = new WWWForm();
        form.AddField("Content-Type", "application/json");

        //e.g. http://192.168.1.216/light/singlepixel/toggle
        var request = UnityWebRequest.Post(this.toggleUrl, form);
        StartCoroutine(OnResponse(request));
    }

    private IEnumerator OnResponse(UnityWebRequest request)
    {
        yield return request.SendWebRequest();
        if(request.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log("Error during sending request: " + request.GetResponseHeader(""));
        }
        else
        {
            Debug.Log("Success " + request.downloadHandler.text);
        }
    }

    void OnSliderValueChanged(PinchSlider slider)
    {
        Debug.Log("Slider value: " + slider.SliderValue + " of slider " + slider.name);
        if(slider == channelRed)
        {
            this.channelRedValue = SliderValueToChannelBrightness(slider.SliderValue);
        } 
        else if(slider == channelBlue)
        {
            this.channelBlueValue = SliderValueToChannelBrightness(slider.SliderValue);
        }
        else if(slider == channelGreen)
        {
            this.channelGreenValue = SliderValueToChannelBrightness(slider.SliderValue);
        }

        string formattedRestUrl = string.Format(this.colorUrl + "?r={0}&g={1}&b={2}", this.channelRedValue, this.channelGreenValue, this.channelBlueValue);
        Debug.Log("Sending request: " + formattedRestUrl);

        WWWForm form = new WWWForm();
        form.AddField("Content-Type", "application/json");

        //e.g. http://192.168.1.216/light/singlepixel/turn_on?r=255&g=255&r=255
        var request = UnityWebRequest.Post(formattedRestUrl, form);
        StartCoroutine(OnResponse(request));
    }

    private int SliderValueToChannelBrightness(float sliderValue)
    {
        return Mathf.RoundToInt(sliderValue * 255);
    }
}
