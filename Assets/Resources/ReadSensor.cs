using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;
using Assets.Resources;
using System;
using UnityEngine.Scripting;

public class ReadSensor : MonoBehaviour
{
    // e.g. http://192.168.1.216/sensor/playground_temperatur
    [SerializeField]
    private string temperatureUrl;

    [SerializeField]
    private string temperatureSensorId;

    //e.g. http://192.168.1.216/sensor/playground_luftdruck
    [SerializeField]
    private string pressureUrl;

    [SerializeField]
    private string pressureSensorId;

    [SerializeField]
    private Interactable toggleButton;

    [SerializeField]
    private TextMeshPro temperatureText;

    [SerializeField]
    private TextMeshPro pressureText;

    void Start()
    {
        this.toggleButton.OnClick.AddListener(delegate () { RefreshReadings(); });
    }

    void RefreshReadings()
    {
        Debug.Log("Fetching sensor values");
        var temperatureRequest = UnityWebRequest.Get(this.temperatureUrl);
        var pressureRequest = UnityWebRequest.Get(this.pressureUrl);
        StartCoroutine(OnResponseSensorRequest(temperatureRequest, pressureRequest));
    }

    private IEnumerator OnResponseSensorRequest(UnityWebRequest temperatureRequest, UnityWebRequest pressureRequest)
    {
        yield return temperatureRequest.SendWebRequest();
        HandleSensorResponse(temperatureRequest, this.temperatureSensorId, "Temperature: {0}", this.temperatureText);
        Debug.Log("Now waiting before sending second request");
        yield return new WaitForSeconds(0.5f);

        yield return pressureRequest.SendWebRequest();
        HandleSensorResponse(pressureRequest, this.pressureSensorId, "Barometer: {0}", this.pressureText);
        yield return new WaitForSeconds(0.5f);
    }

    private void HandleSensorResponse(UnityWebRequest sensorRequest, string expectedSensorId, string textTemplate, TextMeshPro targetText)
    {
        if (sensorRequest.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log("Error during sending request : " + sensorRequest.GetResponseHeader(""));
        }
        else
        {
            Debug.Log("Success " + sensorRequest.downloadHandler.text);
            SensorValues replyParsed = JsonUtility.FromJson<SensorValues>(sensorRequest.downloadHandler.text);
            if (expectedSensorId.Equals(replyParsed.id))
            {
                targetText.text = string.Format(textTemplate, replyParsed.state);
            }
        }
    }
}

