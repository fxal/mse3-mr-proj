using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine.UI;

public class ColorSlider : MonoBehaviour
{
    private PinchSlider slider;

    // Start is called before the first frame update
    void Start()
    {
        this.slider = gameObject.GetComponent<PinchSlider>();
        this.slider.OnValueUpdated.AddListener(delegate { onSliderValueChanged(); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void onSliderValueChanged()
    {
        Debug.Log("Slider value: " + this.slider.SliderValue + " of slider " + this.slider.name);
    }
}
