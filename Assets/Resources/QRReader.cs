using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZXing;
using Vuforia;

[AddComponentMenu("System/VuforiaScanner")]
public class QRReader : MonoBehaviour
{
    [SerializeField]
    private bool cameraActive;

    [SerializeField]
    private int readOnEveryNthFrame;

    private int frameCounter = 0;

    private BarcodeReader barcodeReader;

    private WebCamDevice[] devices;

    private WebCamTexture webCamTexture;

    void Start()
    {
        this.devices = WebCamTexture.devices;
        System.Array.ForEach(this.devices, device => Debug.Log("Found camera: " + device.name 
            + " that is " + (device.isFrontFacing ? "frontfacing" : "not frontfacing")));


        var barcodeFormats = new List<BarcodeFormat>();
        barcodeFormats.Add(BarcodeFormat.QR_CODE);

        this.barcodeReader = new BarcodeReader
        {
            AutoRotate = false,
            Options = new ZXing.Common.DecodingOptions
            {
                PossibleFormats = barcodeFormats,
                TryHarder = true
            }
        };


        Debug.Log("Starting webcam capture (webcamtexture)");
        this.webCamTexture = new WebCamTexture(this.devices[0].name);
        this.webCamTexture.Play();

        Debug.Log("Webcamtexture successfully started: " + this.webCamTexture.isPlaying);
    }

    void Update()
    {
        if(this.cameraActive
            && this.webCamTexture != null 
            && this.webCamTexture.isPlaying)
        {
            if(this.frameCounter >= this.readOnEveryNthFrame)
            {
                Debug.Log("Trying to find QR Code");
                this.frameCounter = 0;

                Result result = this.barcodeReader.Decode(
                    this.webCamTexture.GetPixels32(),
                    this.webCamTexture.width,
                    this.webCamTexture.height);

                if (result != null)
                    Debug.Log("Decoded QR Code: " + result.Text);
            }

            this.frameCounter += 1;
        }        
    }

    private void OnDestroy()
    {
        if (this.webCamTexture != null)
            this.webCamTexture.Stop();
    }
}
